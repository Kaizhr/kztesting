
'''
Try to validate a cityGML file.
Reference:
Hugo Ledoux, https://github.com/tudelft3d/val3dity;
Hugo Ledoux, https://github.com/tudelft3d/CityGML2OBJs;
Hugo ledoux, https://github.com/cityjson/cjio;
Hugo Ledoux, http://geovalidation.bk.tudelft.nl/val3dity/docs/errors

 Validation error code:
 LinearRing Level:
 101 TOO_FEW_POINTS
 102 CONSECUTIVE_POINTS_SAME
 103 NOT_CLOSED
 104 SELF INTERSECTION
 105 COLLAPSED_TO_LINE


Current progress:
	Only get where the invalid building is, don't know the exact ID for the invalid polygon.
Maybe later on, we will try to delete the invalid building from the xml (build a new xml that is 100% valid)
'''

import xml.etree.ElementTree as ET
import numpy as np
from pyproj import Proj, transform
import multiprocessing as mp
from multiprocessing import Process, Manager, Queue, Pool
import time
import sys
import math
from collections import Counter

# Classes for storing data extracted from xml and transformation made by pyproj;
# Use the ".name" property as a reference to find the right place in the original XML file.
class _Edge:
	def __init__(self,head,tail):
		self.head = head
		self.tail = tail
#end class _Edge, we don't care the direction (who is head\tail). Just two points there.

class _Geometry:
	def __init__(self,name,posList):
		self.name = name
		self.posList = posList
		self.cmt = "Valid"

class _Building:
	def __init__(self,name):
		self.name = name
		self.roof = []
		self.foot = []
		self.wall = []	
		self.cmt = "Valid"	
#end class _Building

# read xml and save it to cityObjs
def readCityGML(fileName,_nameSpace):
	buildingList = []
	tree = ET.parse(fileName)
	root = tree.getroot()
	for bldg in root.findall('.//bldg:Building',_nameSpace):
		# find the cityObjectMember's name and create an object of Class _Building
		kiminonamae = bldg.attrib
		#print("kiminonamae = ",kiminonamae)
		# The name is the entire attribute of <bldg:Building>, 
		# for example, {'{http://www.opengis.net/gml}id': 'DENW20AL00006aAC'}
		Building = _Building(kiminonamae)
		# seearch the XML file using the XPath format, provided by ElementTree.
		# and change the string into float arrays, stored in the Building object.
		for roof in bldg.findall('.//bldg:RoofSurface',_nameSpace):
			for Pts in roof.findall('.//gml:posList',_nameSpace):
				posList = np.array(str(Pts.text).split(' '))
				posList = posList.astype(np.float)
				newGeometry = _Geometry(roof.attrib, posList)
				Building.roof.append(newGeometry)	
		for foot in bldg.findall('.//bldg:GroundSurface',_nameSpace):
			for Pts in foot.findall('.//gml:posList',_nameSpace):
				posList = np.array(str(Pts.text).split(' '))
				posList = posList.astype(np.float)
				newGeometry = _Geometry(foot.attrib,posList)
				Building.foot.append(newGeometry)
		for wall in bldg.findall('.//bldg:WallSurface',_nameSpace):
			for Pts in wall.findall('.//gml:posList',_nameSpace):
				posList = np.array(str(Pts.text).split(' '))
				posList = posList.astype(np.float)
				newGeometry = _Geometry(wall.attrib,posList)
				Building.wall.append(newGeometry)
		# Append the object Building to our reserved list.
		buildingList.append(Building)
	# end loop of XML searching
	return buildingList

# 102: check if there is consecutive same points (CPS)?? 
# Actually it checks for points in a ring that are repeated.
# Thus, it should be called as REPEATED_POINTS
def isPolyCPS(polypoints):
	points = polypoints[:-1] # take out the last point, which is same as the first.
	seen = []
	for pt in points:
		if pt in seen:
			return True
		else:
			seen.append(pt)
	return False

# Orientation determination
# from Jonathan Richard Schewchuk, http://www.cs.cmu.edu/~quake/robust.html
# Evaluating the sign of a determinant:
#     | A.x-C.x   A.y-C.y |
#     | B.x-C.x   B.y-C.y |
# which is the volume of vector CA, and CB.
# if the vloume is ZERO, then C is on line AB;
# if the volume is positive, then C is on the left side of line AB; negative for the right side.
def orientation(A,B,C,demension):
	if demension == "x":
		res = (A[1]-C[1])*(B[2]-C[2])-(B[1]-C[1])*(A[2]-C[2])
	elif demension == "y":
		res = (A[0]-C[0])*(B[2]-C[2])-(B[0]-C[0])*(A[2]-C[2])
	elif demension == "z":
		res = (A[0]-C[0])*(B[1]-C[1])-(B[0]-C[0])*(A[1]-C[1])
	if res==0:
		#print("Third point on the line.")
		return 0
	elif res > 0:
		#print("Third point on the left of line")
		return -1
	else:
		#print("Third point on the right of line")
		return 1

# check if the third point is on the segment of AB
def onSegment(A,B,C,demension):
	if demension == "x":
		if (C[1] <= max(A[1], B[1]) and C[1] >= min(A[1], B[1])) and \
		(C[2] <= max(A[2], B[2]) and C[2]>= min(A[2], B[2])): 
			return True; 
	elif demension == "y":
		if (C[0] <= max(A[0], B[0]) and C[0] >= min(A[0], B[0])) and \
		(C[2] <= max(A[2], B[2]) and C[2]>= min(A[2], B[2])): 
			return True; 
	elif demension == "z":
		if (C[0] <= max(A[0], B[0]) and C[0] >= min(A[0], B[0])) and \
		(C[1] <= max(A[1], B[1]) and C[1]>= min(A[1], B[1])): 
			return True; 
	return False

# >> Line Segment Intersection Algorithm,\
# from Bryce Boe https://bryceboe.com/2006/10/23/line-segment-intersection-algorithm
# Segment AB and segment CD intersected, if and only if
# A and B are on different side of line CD, and at the same time, C and D are seperated by line AB as well.
# Additionally, pay attention to tht extrme circumstance, that AB and CD are on a same line.
# That is an invalid case (intersected), if they overlap (valid if not).
# But the results of orientation() give ZERO for both cases.

# Unfortunately, points are in 3-demension, so we have to check the intersection at xy, yz, xz planes.
# True intersection happens to all three planes at the same time.
def isSegIntersected(A,B,C,D,demension):
	orientation_CD_A = orientation(C,D,A,demension)
	orientation_CD_B = orientation(C,D,B,demension)
	orientation_AB_C = orientation(A,B,C,demension)
	orientation_AB_D = orientation(A,B,D,demension)
	# Normal cases: A&B are seperated by CD, while C&D are seperated by AB.
	if (orientation_CD_A != orientation_CD_B) and (orientation_AB_C != orientation_AB_D):
		return True
	# for special cases, some points are alighed in a same line.
	elif orientation_CD_A==0 and onSegment(C,D,A,demension):
		return True
	elif orientation_CD_B==0 and onSegment(C,D,B,demension):
		return True
	elif orientation_AB_C==0 and onSegment(A,B,C,demension):
		return True
	elif orientation_AB_D==0 and onSegment(A,B,D,demension):
		return True
	else:
		return False

# Check two edges if they are joint
def isEdgeConnected(edge1,edge2):
	if edge1.head == edge2.head or edge1.head == edge2.tail:
		return True
	elif edge1.tail == edge2.head or edge1.tail == edge2.tail:
		return True
	else:
		return False
# Check two edges if they are identical
def isEdgeSame(edge1,edge2):
	if edge1.head == edge2.head and edge1.tail == edge2.tail:
		return True
	else:
		return False

# Check two edges if they are intersected, using function isSegIntersected(A,B,C,D):
def isEdgeIntersected(edge1,edge2):
	if isSegIntersected(edge1.head,edge1.tail,edge2.head,edge2.tail,"x") and \
	isSegIntersected(edge1.head,edge1.tail,edge2.head,edge2.tail,"y") and \
	isSegIntersected(edge1.head,edge1.tail,edge2.head,edge2.tail,"z"):
		return True
	else:
		return False

# 104: check self intersection
def isPolySelfIntersected(polypoints):
	edgeList = []
	for i in range(len(polypoints)-1):
		newEdge = _Edge(polypoints[i],polypoints[i+1])
		edgeList.append(newEdge)

	for i in range(len(edgeList)):
		edge = edgeList[i]
		# Ususally, each edge should be connected to other 2 edges.
		# Thus, 1 means not_closed. More than 2 means trouble
		connected_count = 0
		for j in range(len(edgeList)):
			#print("current = ",i," and ",j)
			another_edge = edgeList[j]
			if isEdgeSame(edge,another_edge):
				# skip itself, after filetering by isPolyCPS(), there'll be no repeated edge.
				# Thus, it is used for skiping the exact identical edge ONLY.
				#if j != i:
					#print("Wrong Same [i,j] = ",i," and ",j)
				continue
			elif isEdgeConnected(edge,another_edge):
				# check the connection of the edge. skip its adjacent two edges.
				#print("Connect [i,j] = ",i," and ",j)
				connected_count += 1
				continue
			elif isEdgeIntersected(edge,another_edge):
				# check the edge intersection issues.
				print("Intersected at [i,j]",i," and ",j)
				return True
		#end loop of another_edge
		if connected_count != 2:
			print("Warning! This Edge Connection is problematic. connected_count [",i,"] = ",connected_count)
			return True

	#end loop of edge in edgeList
	return False
# 203: NON_PLANAR_POLYGON_DISTANCE_PLANE
def isPolyPlanar_DSTP(polypoints,tol):
	distance = 0.0
	tempDis = 0.0
	p0 = np.array(polypoints[0])
	p1 = np.array(polypoints[1])
	p2 = np.array(polypoints[2])
	for t in range(3,len(polypoints)-1):
		pt = np.array(polypoints[t])
		distance = calculateDistanceToPlane(p0,p1,p2,pt)
		if distance > tol:
			if tempDis < distance:
				tempDis = distance

	distance = tempDis
	return distance

# Calculate the distance from point pt to plane p0p1p2.
def calculateDistanceToPlane(p0,p1,p2,pt):
	normal = np.cross(p1-p0,p2-p0)
	magnitude = np.linalg.norm(normal)
	if magnitude ==0:
		print("WARNING, divide by ZERO:",normal)
	normal = np.true_divide(normal,magnitude)
	distance = np.absolute(np.dot(normal,pt-p0))
	return distance

	return distance
# 204: NON_PLANAR_POLYGON_NORMALS_DEVIATION, return the max angel deviation in degree.
def isPolyPlanar_NORMAL(polypoints,tol):
	#-- Normal of the polygon from the first three points
	AngleDeviation = 0
	tempDev = 0
	#-- Number of points, last point is as same as the first.
	npolypoints = len(polypoints)
	# after filtering by Error code 101, a polygon at least has 4 point: p0 p1 p2 ~ p3
	p0 = np.array(polypoints[0])
	p1 = np.array(polypoints[1])
	p2 = np.array(polypoints[2])
	for nt in range(3,npolypoints-1):
		p3 = np.array(polypoints[nt])
		AngleDeviation = calculateAngleDeviation(p0,p1,p2,p3)
		if AngleDeviation > tol:
			if tempDev < AngleDeviation:
				tempDev = AngleDeviation

	AngleDeviation = tempDev
	return AngleDeviation

# calculate the angle between p3p0 and the normal of plane p0p1p2, 
# then calculate the deviation to 90 degree.
def calculateAngleDeviation(p0,p1,p2,p3):
	# get the normal of plane p0p1p2
	normal = np.cross(p1-p0,p2-p0)
	magnitude = np.linalg.norm(normal)
	if magnitude ==0:
		# this means p0,p1,p2 are on the same line;
		# Thus we have to change one of these 3 points. For example, replace p2 with someone else.
		# Obviously, AngleDeviation should be greater than or equal to zero. -1 means something wrong.
		return -1
	normal = np.true_divide(normal,magnitude)
	# get the vector p3p0
	vector = p3-p0
	magnitude = np.linalg.norm(vector)
	if magnitude ==0:
		print("WARNING, divide by ZERO:",vector)
	vector = np.true_divide(vector,magnitude)
	# get angle and deviation to 90 degree
	AngleDeviation = np.arccos(np.clip(np.dot(vector,normal), -1.0, 1.0))/np.pi*180
	AngleDeviation = np.absolute(AngleDeviation-90)
	return AngleDeviation

# Check all Errors
def isPolyValid(polypoints):
	#-- Number of points of the polygon (including the doubled first/last point)
	npolypoints = len(polypoints)
	#-- Assume that it is valid, and try to disprove the assumption
	valid = ""
	# 101 - TOO_FEW_POINTS
	# Four because the first point is doubled as the last one in the ring
	if npolypoints < 4:
		valid += "Invalid: 101 TOO_FEW_POINTS.\n"
	# 102 – CONSECUTIVE_POINTS_SAME: Points in a ring should not be repeated 
	if isPolyCPS(polypoints):
		valid += "Invalid: 102 CONSECUTIVE_POINTS_SAME.\n"

	# 103 – RING_NOT_CLOSED: Check if last point equal
	if polypoints[0] != polypoints[-1]:
		valid += "Invalid: 103 NOT_CLOSED.\n"
	
	if isPolySelfIntersected(polypoints):
		valid += "Invalid: 104: RING_SELF_INTERSECTION.\n"
		pass
	# -- Check if the points are planar, 203 and 204:
	# 203 – NON_PLANAR_POLYGON_DISTANCE_PLANE
	tol_in_degree = 0.1
	distance_deviation = isPolyPlanar_DSTP(polypoints,tol_in_degree)
	if distance_deviation > tol_in_degree:
		valid += "Invalid: 203 NON_PLANAR_POLYGON_DISTANCE_PLANE. Distance_Deviation_in_Degree(Geodetic) = "+\
		str(distance_deviation)+"\n"
	# 204 – NON_PLANAR_POLYGON_NORMALS_DEVIATION
	tol_in_degree = 9
	angle_deviation = isPolyPlanar_NORMAL(polypoints,tol_in_degree) 
	if angle_deviation > tol_in_degree:
		valid += "Invalid: '204 NON_PLANAR_POLYGON_NORMALS_DEVIATION. Angle_Deviation_in_Degree = "+\
		str(angle_deviation)+"\n"

	# -----------------
	# Without issues.
	if valid == "":
		valid += "Valid"
	return valid

# Convert a poslist to a polygon, which is an array of points, Like, [[x,y,z],...]
def polygonConverter(posList):
	polygon = []
	for i in range(int(len(posList)/3)):
		pt = [posList[3*i],posList[3*i+1],posList[3*i+2]]
		polygon.append(pt)
	return polygon

# Validation 
def validation(buildingList,invalidResult,loc):
	print("process starts = ",loc)
	proxy = buildingList[loc]
	# roof
	for rj in range(len(proxy.roof)):
		polygon = polygonConverter(proxy.roof[rj].posList)
		res = isPolyValid(polygon)
		if res != "Valid":
			proxy.cmt = "Invalid"
		proxy.roof[rj].cmt = res		
	# foot
	for fj in range(len(proxy.foot)):
		polygon = polygonConverter(proxy.foot[fj].posList)
		res = isPolyValid(polygon)
		if res != "Valid":
			proxy.cmt = "Invalid"
		proxy.foot[fj].cmt = res
	# wall
	for wj in range(len(proxy.wall)): 
		polygon = polygonConverter(proxy.wall[wj].posList)
		res = isPolyValid(polygon)
		if res != "Valid":
			proxy.cmt = "Invalid"
		proxy.wall[wj].cmt = res

	invalidResult.append(proxy)
	print("process ends = ",loc)
	return 0

def main():
	# deal with fileNames
	if len(sys.argv) == 1:
		# Default fileNames
		fileName = "DUSSELDORF-5-building-test.xml"
		#fileName = "LoD2_354_5667_1_NW.gml.xml"
		fileName_exported = "report.txt"
	elif len(sys.argv) == 2:
		if sys.argv[1] == "-H" or sys.argv[1] == "--help":
			print("-----------------------------------------------------")
			print("python xmlParser_Process.py [input.xml] [report.txt]")
			print("-----------------------------------------------------")
			return 0
		else:
			print("Use \"-H\" or \"--help\" for more information")
			return 0
	elif len(sys.argv) == 3:
		fileName = sys.argv[1]
		fileName_exported = sys.argv[2]
	else:
		print(">>>Error: TOO MANY ARGUMENTS.")
		print("-----------------------------------------------------")
		print("python xmlParser_Process.py [input.xml] [report.txt]")
		print("-----------------------------------------------------")
		return 0

	# nameSpace used for ElementTree's search function
	_nameSpace = {'core':"http://www.opengis.net/citygml/1.0",
	'gen':"http://www.opengis.net/citygml/generics/1.0",
	'grp':"http://www.opengis.net/citygml/cityobjectgroup/1.0",
	'app':"http://www.opengis.net/citygml/appearance/1.0",
	'bldg':"http://www.opengis.net/citygml/building/1.0",
	'gml':"http://www.opengis.net/gml",
	'xal':"urn:oasis:names:tc:ciq:xsdschema:xAL:2.0",
	'xlink':"http://www.w3.org/1999/xlink",
	'xsi':"http://www.w3.org/2001/XMLSchema-instance"}

	# save the xml data into buildingList. To call the building inside it, 
	# use buildingList[x].name or buildingList[x].roof (.foor or .wall) 
	buildingList = readCityGML(fileName,_nameSpace)
	print("Number buildings = ",len(buildingList))


	# prepare for multiprocessing
	# manager.list() is one of several ways that are only available for exchanging data between multple processes. 
	manager = Manager()
	invalidResult = manager.list()

	# You may try Pool(), see more details in xmlParser_Process.py
	pool = Pool()
	pool.starmap(validation,[(buildingList,invalidResult,loc) for loc in range(len(buildingList))])
	pool.close()
	pool.join()

	# About results
	report_str = ""
	num_invalid_geometry = 0
	num_invalid_building = 0
	for i in range(len(invalidResult)):
		invalidOne = invalidResult[i]
		if invalidOne.cmt == "Valid":
			continue
		else:
			num_invalid_building += 1
			for roof in invalidOne.roof:
				if roof.cmt != "Valid":
					num_invalid_geometry += 1
					newInvalidStr = str(num_invalid_geometry)+". In Building = "+str(invalidOne.name)+\
					";\n Roof Issue = "+str(roof.name)+" "+str(roof.cmt)+"\n"	
					#print("Building No ",i,">>",newInvalidStr)
					report_str += newInvalidStr
			for foot in invalidOne.foot:
				if foot.cmt != "Valid":
					num_invalid_geometry += 1
					newInvalidStr = str(num_invalid_geometry)+". In Building = "+str(invalidOne.name)+\
					";\n Foot Issue = "+str(foot.name)+" "+str(foot.cmt)+"\n"	
					#print("Building No ",i,">>",newInvalidStr)
					report_str += newInvalidStr
			for wall in invalidOne.wall:
				if wall.cmt != "Valid":
					num_invalid_geometry += 1
					newInvalidStr = str(num_invalid_geometry)+". In Building = "+str(invalidOne.name)+\
					";\n Wall Issue = "+str(wall.name)+" "+str(wall.cmt)+"\n"	
					#print("Building No ",i,">>",newInvalidStr)
					report_str += newInvalidStr


	print("number of invalid LinearRings = ",num_invalid_geometry)
	print("cpu_count = ",mp.cpu_count())
	print("number of invalid Buildings = ",num_invalid_building,"/",len(invalidResult))
		
	if num_invalid_building == 0:
		report_str = "All buildings are valid!"
	else:
		report_str += "---------------------------------------------------------------------------"
		report_str += "\n Number of invalid LinearRings = "+str(num_invalid_geometry)+"\n"
		report_str += "\n Number of invalid Buildings = "+str(num_invalid_building)+"/"+str(len(invalidResult))+"\n"
	

	with open(fileName_exported,'w') as f_handle:
		f_handle.write(report_str)

if __name__ == '__main__':
	start_time = time.time()
	main()
	print("--- %.6f seconds ---" % (time.time() - start_time))