'''
Once you find some invalid primitives, check it here in a picture. See if it is really a wrong one.
'''

import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import math
#-- Validity of a polygon, from CityGML2OBJs ---------
def det(a):
    """Determinant of matrix a. only in 3D"""
    return a[0][0]*a[1][1]*a[2][2] + a[0][1]*a[1][2]*a[2][0] + a[0][2]*a[1][0]*a[2][1] \
    - a[0][2]*a[1][1]*a[2][0] - a[0][1]*a[1][0]*a[2][2] - a[0][0]*a[1][2]*a[2][1]
# from CityGML2OBJs ---------
def dot(a, b):
    """Dot product of vectors a and b."""
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2]
# from CityGML2OBJs ---------
def unit_normal(a, b, c):
    """Unit normal vector of plane defined by points a, b, and c."""
    x = det([[1,a[1],a[2]],
             [1,b[1],b[2]],
             [1,c[1],c[2]]])
    y = det([[a[0],1,a[2]],
             [b[0],1,b[2]],
             [c[0],1,c[2]]])
    z = det([[a[0],a[1],1],
             [b[0],b[1],1],
             [c[0],c[1],1]])
    magnitude = (x**2 + y**2 + z**2)**.5
    if magnitude == 0.0:
        raise SyntaxWarning("The normal of the polygon has no magnitude. Check the polygon. \
            The most common cause for this are two identical and sequential points.")
    return [x/magnitude, y/magnitude, z/magnitude]
    #return [x,y,z]

# 203： NON_PLANAR_POLYGON, from CityGML2OBJs ---------
def isPolyPlanar(polypoints):
    """Checks if a polygon is planar."""
    #-- Normal of the polygon from the first three points
    normal = unit_normal(polypoints[0], polypoints[1], polypoints[2])
    #-- Number of points
    npolypoints = len(polypoints)
    #-- Tolerance
    eps = 0.01
    #-- Assumes planarity
    planar = True
    for i in range (3, npolypoints-1):
        vector = [polypoints[i][0] - polypoints[0][0], polypoints[i][1] - polypoints[0][1], polypoints[i][2] - polypoints[0][2]]
        magnitude = (vector[0]**2 + vector[1]**2 + vector[2]**2)**.5
        vector = [vector[0]/magnitude,vector[1]/magnitude,vector[2]/magnitude]
        res_dot = math.fabs(dot(vector, normal))
        if res_dot > eps:
            print("vector = ",vector)
            print("normal = ",normal)
            print(res_dot)
            planar = False
    return planar

if __name__ == '__main__':
    # search your surface by the gml:id in report.txt;
    # and paste the points here
    data = "354124.405 5667002.316 49.945 354123.518 5667004.562 52.238 354124.405 5667002.316 49.913 354124.405 5667002.316 43.722 354124.405 5667002.316 49.945"
    posList = np.array(str(data).split(' '))
    #print(posList)
    fig = plt.figure(dpi=800)
    ax1 = fig.add_subplot(111,projection='3d')
    x = []
    y = []
    z = []
    polygon = []
    colorBar = ['red','darkorange','gold','darkgreen','deepskyblue','blue','darkorchid']
    for i in range(int(len(posList)/3)):
        x.append(float(posList[3*i]))
        y.append(float(posList[3*i+1]))
        z.append(float(posList[3*i+2]))
        pt = [float(posList[3*i]),float(posList[3*i+1]),float(posList[3*i+2])]
        polygon.append(pt)
        j = i % 7
        print("j = ",colorBar[j])
        ax1.scatter(pt[0],pt[1],pt[2],color=colorBar[j],lw=10-j,alpha = 0.5)
    #verts = [list(zip(x,y,z))]
    print("is Planar? ",isPolyPlanar(polygon))
    print("num of point in polygon = ",len(polygon))
    ax1.plot(x,y,z,color='darkorange',lw=1,alpha = 0.5)
    fig.savefig("testing_problematic_Surface.png")

    


