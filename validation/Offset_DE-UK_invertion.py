#!/usr/bin/env python
# coding: utf-8

# In[1]:


# -*- coding: utf-8 -*-
"""
Modified on mwi's code: parse_roof_area.py, basically the part of using pyproj to transform projected CRSs.
"""

import xml.etree.ElementTree as ET
import numpy as np
from pyproj import Proj, transform
import re
import math
import scipy.linalg
#import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import multiprocessing
from multiprocessing import Process
import time
import copy
#import polygon3dmodule as pol3d


'''
This script moves a gml from its original country/area to a target location.
'''

'''
pyproj CRS Parameter:

ESPG:5834
DB_REF / 3-degree Gauss-Kruger zone 4 (E-N) + DHHN92 height

ESPG:5555
ETRS89 / UTM zone 32N + DHHN92 height

EPSG:23032
ED50 / UTM zone 32N -- Denmark

EPSG:27700
OSGB 1936 / British National Grid -- United Kingdom Ordnance Survey

'''

# Parse the XML file
def parse_GML(data):
    global surface_list
    global foot_list
    global wall_list
    for line in range(len(data)):
        # Polygons
        if "<bldg:lod0RoofEdge" in str(data[line]) or "<bldg:RoofSurface" in str(data[line]):
            for k in range(len(data)-line):
                if "gml:posList" in str(data[line+k]):
                    roof_surface = []
                    roof_temp = data[line+k]
                    roof_temp = re.sub('<[^>]+>', '', roof_temp)
                    bb_r = roof_temp.split(" ")
                    for k in range(int(len(bb_r)/3)):
                        point = np.array([bb_r[3*k], bb_r[3*k+1], bb_r[3*k+2]])
                        roof_surface.append(point)
                    break
            surface_list.append(roof_surface)
        elif "<bldg:lod0FootPrint" in str(data[line]) or "<bldg:GroundSurface" in str(data[line]):
            for k in range(len(data)-line):
                if "gml:posList" in str(data[line+k]):
                    foot = []
                    foot_temp = data[line+k]
                    foot_temp = re.sub('<[^>]+>', '', foot_temp)
                    bb_f = foot_temp.split(" ")
                    for k in range(int(len(bb_f)/3)):
                        point = np.array([bb_f[3*k], bb_f[3*k+1], bb_f[3*k+2]])
                        foot.append(point)
                    break
            foot_list.append(foot)
        elif "<bldg:lod0Waterwall" in str(data[line]) or "<bldg:WallSurface" in str(data[line]):
            for k in range(len(data)-line):
                if "gml:posList" in str(data[line+k]):
                    wall = []
                    wall_temp = data[line+k]
                    wall_temp = re.sub('<[^>]+>', '', wall_temp)
                    bb_f = wall_temp.split(" ")
                    for k in range(int(len(bb_f)/3)):
                        point = np.array([bb_f[3*k], bb_f[3*k+1], bb_f[3*k+2]])
                        wall.append(point)
                    break
            wall_list.append(wall)
    print("Extracted " + str(len(surface_list)) + " roof surfaces.")
    print("Extracted " + str(len(foot_list)) + " foot prints.")
    print("Extracted " + str(len(wall_list))+ " wall surfaces.")
    return 0
# end def

'''
Transform coordinates and there are 
TWO ways to move a building from one place to another:

I. The first one is to move the buildings directly from (x,y,z) to (x',y',z'), regardless of CRS difference. This method might avoid the potential distortion from the pyrpoj transformation.
1) Pick a REF point from the input GML file;
2) Calculate the relative positions for all the points in GML file:
        x_r = x_o - x_REF;
3) All the rest of points can be calculated as:
        x_new = x_r + x_tgt = x_tgt + x_o -x_REF
4) Draw the x_new points in the target CRS, to see whether they are distorted or not;
Since we don't transform any point, the computation speed will be really fast.

II. Another method is to transform all the points to the target CRS first, and then add the offset. Undoubtedly, it will be super slow:
1) x_tsf = transform(x_o);
2) offset = x_tgt - transform(x_REF);
3) x_new = x_tsf + offset

You might compare the result of these 2 methods.

'''
# method I:
def methodQuick(surface_list,foot_list,wall_list,targetLoc,
    gps_list_roof,gps_list_roof_transformed,gps_list_foot,gps_list_foot_transformed,
    gps_list_wall,gps_list_wall_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj,):
    
    # transform'em
    pt_REF = foot_list[0][0]
    pt_tgt = targetLoc
    p_Roof = Process(target=transform_RoofSurface, args=(1,surface_list,pt_REF,pt_tgt,
        gps_list_roof,gps_list_roof_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj,))
    p_Foot = Process(target=transform_GroundSurface, args=(1,foot_list,pt_REF,pt_tgt,
        gps_list_foot,gps_list_foot_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj,))
    p_Wall = Process(target=transform_WallSurface, args=(1,wall_list,pt_REF,pt_tgt,
        gps_list_wall,gps_list_wall_transformed,inProj,outProj,))
    
    p_Roof.start()
    p_Foot.start()
    p_Wall.start()
    p_Roof.join()
    p_Foot.join()
    p_Wall.join()
    
    return 0

# method II:
def offsetMove(pt_tgt,gps_list_roof_transformed,gps_list_foot_transformed,gps_list_wall_transformed,
    minRange_in,maxRange_in,minRange_out,maxRange_out):
    # Pick a REF point from gps_list_foot_transformed
    pt_REF = gps_list_foot_transformed[0][0]
    print("transformed pt_REF = ",pt_REF)
    # Calculate the offset in [x,y,z]
    OFFSET = np.subtract(np.array(pt_tgt),np.array(pt_REF))
    OFFSET[2] = 0
    
    # Move'em all. Only x & y, not z-axis.
    for s in range(len(gps_list_roof_transformed)):
        proxy = gps_list_roof_transformed[s]
        for p in range(len(gps_list_roof_transformed[s])):
            proxy[p][0] += OFFSET[0]
            proxy[p][1] += OFFSET[1]
        gps_list_roof_transformed[s] = proxy  
        
    for s in range(len(gps_list_foot_transformed)):
        proxy = gps_list_foot_transformed[s]
        for p in range(len(gps_list_foot_transformed[s])):         
            proxy[p][0] += OFFSET[0]
            proxy[p][1] += OFFSET[1]
            gps_list_foot_transformed[s] = proxy
    
    proxy = gps_list_wall_transformed
    for s in range(len(gps_list_wall_transformed)):
        proxy = gps_list_wall_transformed[s]
        for p in range(len(gps_list_wall_transformed[s])):
            proxy[p][0] += OFFSET[0]
            proxy[p][1] += OFFSET[1]
            gps_list_wall_transformed[s] = proxy
    # move boundaries for ax2)
    minRange_out[0] += OFFSET[0]
    minRange_out[1] += OFFSET[1]
    maxRange_out[0] += OFFSET[0]
    maxRange_out[1] += OFFSET[1]
    print("offset = ",OFFSET)
    print("point test = ",gps_list_foot_transformed[0][0])
    return 0
    
def methodTransformAll(surface_list,foot_list,wall_list,targetLoc,
    gps_list_roof,gps_list_roof_transformed,gps_list_foot,gps_list_foot_transformed,
    gps_list_wall,gps_list_wall_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj):
    
        
    # transform'em all
    pt_REF = [0,0,0]
    pt_tgt = targetLoc
    p_Roof = Process(target=transform_RoofSurface, args=(2,surface_list,pt_REF,pt_tgt,
        gps_list_roof,gps_list_roof_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj,))
    p_Foot = Process(target=transform_GroundSurface, args=(2,foot_list,pt_REF,pt_tgt,
        gps_list_foot,gps_list_foot_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj,))
    p_Wall = Process(target=transform_WallSurface, args=(2,wall_list,pt_REF,pt_tgt,
        gps_list_wall,gps_list_wall_transformed,inProj,outProj,))
    p_Roof.start()
    p_Foot.start()
    p_Wall.start()
    p_Roof.join()
    p_Foot.join()
    p_Wall.join()
    
    p_Offset = Process(target=offsetMove,args=(pt_tgt,gps_list_roof_transformed,gps_list_foot_transformed,
        gps_list_wall_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,))
    p_Offset.start()
    p_Offset.join()

    return 0
    
def transform_RoofSurface(method,surface_list,pt_REF,pt_tgt,
    gps_list_roof,gps_list_roof_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj):    
# Roof: save original and transformed points into gps_list_roof, gps_list_roof_transformed
# And Find the max_z only
    
    initSign_roof = 0

    for s in range(len(surface_list)):
        print("ROOF SURFACE " + str(s) + " (" + str(len(surface_list[s])) + " points)")
        gps_surface = []
        gps_surface_transformed = []
        for p in range(len(surface_list[s])):
            if method == 1:
                res_x = float(pt_tgt[0])+float(surface_list[s][p][0])-float(pt_REF[0])
                res_y = float(pt_tgt[1])+float(surface_list[s][p][1])-float(pt_REF[1])
            elif method == 2:
                #pyproj transform CRS 
                res_x,res_y = transform(inProj,outProj,surface_list[s][p][0],surface_list[s][p][1])
                '''
                # test the pyproj transform function: is the transformation results invertible?
                # Answer: yes, it is.
                res_x,res_y = transform(outProj,inProj,res_x,res_y)
                res_x,res_y = transform(inProj,outProj,res_x,res_y)
                '''
            #add the z-axis value
            gps_point = [float(surface_list[s][p][0]),float(surface_list[s][p][1]), float(surface_list[s][p][2])]
            gps_point_transformed = [res_x, res_y, float(surface_list[s][p][2])]
            #append the point to the lists: gps_surface, gps_surface_transformed
            gps_surface.append(np.array(gps_point))
            gps_surface_transformed.append(np.array(gps_point_transformed))
        # append to the list of all roofs
        gps_list_roof.append(np.array(gps_surface))
        gps_list_roof_transformed.append(np.array(gps_surface_transformed))
        
        if initSign_roof == 0:
            initSign_roof = 1
            temp_max_z_in = max(np.array(gps_surface)[:,2])
            temp_max_z_out = max(np.array(gps_surface_transformed)[:,2])
        else:
            if temp_max_z_in < max(np.array(gps_surface)[:,2]):
                temp_max_z_in = max(np.array(gps_surface)[:,2])
            if temp_max_z_out < max(np.array(gps_surface_transformed)[:,2]):
                temp_max_z_out = max(np.array(gps_surface_transformed)[:,2])
    # end loop
    
    maxRange_in[2] = temp_max_z_in
    maxRange_out[2] = temp_max_z_out
    
    return 0

def transform_GroundSurface(method,foot_list,pt_REF,
    pt_tgt,gps_list_foot,gps_list_foot_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj):
# Foot: save original and transformed points into gps_list_foot, gps_list_foot_transformed
# And Find the range of coordinates (x,y), and min_z only
    initSign_foot = 0  
    for s in range(len(foot_list)):
        print("FOOT PRINTS " + str(s) + " (" + str(len(foot_list[s])) + " points)")
        gps_foot = []
        gps_foot_transformed = []   
        for p in range(len(foot_list[s])):
            if method == 1:
                res_x = float(pt_tgt[0])+float(foot_list[s][p][0])-float(pt_REF[0])
                res_y = float(pt_tgt[1])+float(foot_list[s][p][1])-float(pt_REF[1])
            elif method == 2:
                #pyproj transform CRS
                res_x,res_y = transform(inProj,outProj,foot_list[s][p][0],foot_list[s][p][1])
                '''
                # test the pyproj transform function: is the transformation results invertible?
                # Answer: yes, it is.
                res_x,res_y = transform(outProj,inProj,res_x,res_y)
                res_x,res_y = transform(inProj,outProj,res_x,res_y)
                '''
                
            #add the z-axis value
            gps_point = np.array([float(foot_list[s][p][0]),float(foot_list[s][p][1]), float(foot_list[s][p][2])]) 
            gps_point_transformed = np.array([res_x, res_y, float(foot_list[s][p][2])]) 
            # append
            gps_foot.append(gps_point)
            gps_foot_transformed.append(gps_point_transformed)
        # append one single foot to the list of foots
        gps_list_foot.append(gps_foot)
        gps_list_foot_transformed.append(gps_foot_transformed)
    
        if initSign_foot == 0:
            initSign_foot = 1
            temp_min_x_in = min(np.array(gps_foot)[:,0])
            temp_min_x_out = min(np.array(gps_foot_transformed)[:,0])
            temp_max_x_in = max(np.array(gps_foot)[:,0])
            temp_max_x_out = max(np.array(gps_foot_transformed)[:,0])

            temp_min_y_in = min(np.array(gps_foot)[:,1])
            temp_min_y_out = min(np.array(gps_foot_transformed)[:,1])
            temp_max_y_in = max(np.array(gps_foot)[:,1])
            temp_max_y_out = max(np.array(gps_foot_transformed)[:,1])

            temp_min_z_in = min(np.array(gps_foot)[:,2])
            temp_min_z_out = min(np.array(gps_foot_transformed)[:,2])
        else:
            if temp_min_x_in > min(np.array(gps_foot)[:,0]):
                temp_min_x_in = min(np.array(gps_foot)[:,0])
            if temp_min_x_out > min(np.array(gps_foot_transformed)[:,0]):
                temp_min_x_out = min(np.array(gps_foot_transformed)[:,0])
            if temp_max_x_in < max(np.array(gps_foot)[:,0]):
                temp_max_x_in = max(np.array(gps_foot)[:,0])
            if temp_max_x_out < max(np.array(gps_foot_transformed)[:,0]):
                temp_max_x_out = max(np.array(gps_foot_transformed)[:,0])

            if temp_min_y_in > min(np.array(gps_foot)[:,1]):
                temp_min_y_in = min(np.array(gps_foot)[:,1])
            if temp_min_y_out > min(np.array(gps_foot_transformed)[:,1]):
                temp_min_y_out = min(np.array(gps_foot_transformed)[:,1])
            if temp_max_y_in < max(np.array(gps_foot)[:,1]):
                temp_max_y_in = max(np.array(gps_foot)[:,1])
            if temp_max_y_out < max(np.array(gps_foot_transformed)[:,1]):
                temp_max_y_out = max(np.array(gps_foot_transformed)[:,1])

            if temp_min_z_in > min(np.array(gps_foot)[:,2]):
                temp_min_z_in = min(np.array(gps_foot)[:,2])
            if temp_min_z_out > min(np.array(gps_foot_transformed)[:,2]):
                temp_min_z_out = min(np.array(gps_foot_transformed)[:,2])
    # end loop, and store the temp_min&max


    maxRange_in[0] = temp_max_x_in
    maxRange_in[1] = temp_max_y_in

    maxRange_out[0] = temp_max_x_out
    maxRange_out[1] = temp_max_y_out
    
    minRange_in[0] = temp_min_x_in
    minRange_in[1] = temp_min_y_in
    minRange_in[2] = temp_min_z_in

    minRange_out[0] = temp_min_x_out
    minRange_out[1] = temp_min_y_out
    minRange_out[2] = temp_min_z_out
    return 0


def transform_WallSurface(method,wall_list,pt_REF,pt_tgt,gps_list_wall,gps_list_wall_transformed,inProj,outProj):
# Wall: save original and transformed points into gps_wall_roof, gps_list_wall_transformed

    for s in range(len(wall_list)):
        print("WALL PRINTS " + str(s) + " (" + str(len(wall_list[s])) + " points)")
        gps_wall = []
        gps_wall_transformed = []
        for p in range(len(wall_list[s])):
            if method == 1:
                res_x = float(pt_tgt[0])+float(wall_list[s][p][0])-float(pt_REF[0])
                res_y = float(pt_tgt[1])+float(wall_list[s][p][1])-float(pt_REF[1])
            elif method == 2:
                #pyproj transform CRS              
                res_x,res_y = transform(inProj,outProj,wall_list[s][p][0],wall_list[s][p][1])
                '''
                # test the pyproj transform function: is the transformation results invertible?
                # Answer: yes, it is.
                res_x,res_y = transform(outProj,inProj,res_x,res_y)
                res_x,res_y = transform(inProj,outProj,res_x,res_y)
                '''
            #add the z-axis value
            gps_point = np.array([float(wall_list[s][p][0]),float(wall_list[s][p][1]), float(wall_list[s][p][2])]) 
            gps_point_transformed = np.array([res_x, res_y, float(wall_list[s][p][2])]) 
            # append
            gps_wall.append(gps_point)
            gps_wall_transformed.append(gps_point_transformed)
        # append it to the list of all walls
        gps_list_wall.append(gps_wall)
        gps_list_wall_transformed.append(gps_wall_transformed)
    return 0



'''
-----------------------------------------
main function.
Do all the Configuration Here.
-----------------------------------------
'''
if __name__ == '__main__':
    start_time = time.time()
    
    '''
    Choose your gml file here:
    '''
    filename = "Building_CityGML_2.0.xml"
    #filename = "FZK-Haus-LoD0-KIT-IAI-KHH-B36-V1.gml.xml"
    filename = "LoD2_354_5667_1_NW.gml.xml"
    filename = "Simplified_LoD2_354_5667_1_NW.gml.xml"
    filename = "testing5.xml"

    '''
    Set your destination location here (in XYZ geocentric coordinates);
    Current setting is a place in London, UK.
    z-axis not considered.
    '''
    targetLoc = [522901.810984, 179591.260805, 0]

    # Open a file
    data = []
    with open(filename) as inputfile:    
        for line in inputfile:
            data.append(line.strip())

    surface_list = []
    foot_list = []
    wall_list = []

    # parse it
    parse_GML(data)
    
    # Store your coordinates
    manager = multiprocessing.Manager()
    
    gps_list_roof = manager.list()
    gps_list_roof_transformed = manager.list()
    gps_list_foot = manager.list()
    gps_list_foot_transformed = manager.list()
    gps_list_wall = manager.list()
    gps_list_wall_transformed = manager.list()
    
    minRange_in = manager.list([0,0,0])
    minRange_out = manager.list([0,0,0])
    maxRange_in = manager.list([0,0,0])
    maxRange_out = manager.list([0,0,0])
    

    '''
    # 1 for the first method; 2 for the second
    '''
    selectedMethod = 2
    
    inProj = Proj(init='epsg:27700')
    outProj = Proj(init='epsg:5555')
    
    if selectedMethod == 1:
        # Method one: quick shift
        p_mthd_1 = Process(target=methodQuick,args=(surface_list,foot_list,wall_list,targetLoc,
            gps_list_roof,gps_list_roof_transformed,gps_list_foot,gps_list_foot_transformed,
            gps_list_wall,gps_list_wall_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj,))
        p_mthd_1.start()
        p_mthd_1.join()
    elif selectedMethod == 2:
        # Method two: transform'em all
        p_mthd_2 = Process(target=methodTransformAll,args=(surface_list,foot_list,wall_list,targetLoc,
            gps_list_roof,gps_list_roof_transformed,gps_list_foot,gps_list_foot_transformed,
            gps_list_wall,gps_list_wall_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,inProj,outProj,))
        p_mthd_2.start()
        p_mthd_2.join()
        #'''
        # if you want to transform it back to Germany. To get the target location in Germany, which is the same 
        # postion in the gps_list_foot[0][0]. 
        pt_tgt_DE =  gps_list_foot[0][0]
        surface_list = copy.deepcopy(gps_list_roof_transformed)
        foot_list = copy.deepcopy(gps_list_foot_transformed)
        wall_list = copy.deepcopy(gps_list_wall_transformed)
        # clear the manager lists, otherwise the inverse operation will append new transformations to the old arrays.
        gps_list_roof = manager.list()
        gps_list_roof_transformed = manager.list()
        gps_list_foot = manager.list()
        gps_list_foot_transformed = manager.list()
        gps_list_wall = manager.list()
        gps_list_wall_transformed = manager.list()

        minRange_in = manager.list([0,0,0])
        minRange_out = manager.list([0,0,0])
        maxRange_in = manager.list([0,0,0])
        maxRange_out = manager.list([0,0,0])


        p_mthd_2_bkDE = Process(target=methodTransformAll,args=(surface_list,foot_list,wall_list,pt_tgt_DE,
            gps_list_roof,gps_list_roof_transformed,gps_list_foot,gps_list_foot_transformed,
            gps_list_wall,gps_list_wall_transformed,minRange_in,maxRange_in,minRange_out,maxRange_out,outProj,inProj,))
        p_mthd_2_bkDE.start()
        p_mthd_2_bkDE.join()
        #'''
    #end elif

    '''
    ------------------------------------------------------
    Plotted by matplotlib:
    ax1 for results of original gml;
    ax2 for results of the selected method;
    
    If you find the building distorted, just change the 
            figsize=(xx,yy);
    where xx, yy are in inches.
    
    This is because the actual scale of x-y-z axis is not identical.
    Current version of matplotlib is not supporting a 3D plot with equal-scaled axis.
    ------------------------------------------------------
    '''

    fig = plt.figure(dpi=800, figsize=(24,8))
    lineWidth = 0.5
    ax1 = fig.add_subplot(121,projection='3d')
    ax2 = fig.add_subplot(122,projection='3d')

    # Draw roofs for ax1 and ax2
    axLabelSign = 0
    for roof_t in gps_list_roof:
        xs = []
        ys = []
        zs = []
        # lines
        for pt in roof_t:
            xs.append(pt[0])
            ys.append(pt[1])
            zs.append(pt[2])
        xs = np.asarray(xs)
        ys = np.asarray(ys)
        zs = np.asarray(zs)
        if axLabelSign == 0:
            ax1.plot(xs,ys,zs,color='firebrick',lw=lineWidth,label='Roofedges')
            axLabelSign = 1
        else:
            ax1.plot(xs,ys,zs,color='firebrick',lw=lineWidth)
        # polygon
        verts = [list(zip(xs,ys,zs))]
        ax1.add_collection(Poly3DCollection(verts,alpha=0.6,facecolor='red'))
        
    axLabelSign = 0
    for roof_t in gps_list_roof_transformed:
        xs = []
        ys = []
        zs = []
        # lines
        for pt in roof_t:
            xs.append(pt[0])
            ys.append(pt[1])
            zs.append(pt[2])
        xs = np.asarray(xs)
        ys = np.asarray(ys)
        zs = np.asarray(zs)
        if axLabelSign == 0:
            ax2.plot(xs,ys,zs,color='firebrick',lw=lineWidth,label='Roofedges')
            axLabelSign = 1
        else:
            ax2.plot(xs,ys,zs,color='firebrick',lw=lineWidth)
        # polygon
        verts = [list(zip(xs,ys,zs))]
        ax2.add_collection(Poly3DCollection(verts,alpha=0.6,facecolor='red'))
    
    # draw groundSurfaces
    axLabelSign = 0
    for foot in gps_list_foot:
        xs = []
        ys = []
        zs = []
        for pt in foot:
            xs.append(pt[0])
            ys.append(pt[1])
            zs.append(pt[2]) 
        xs = np.asarray(xs)
        ys = np.asarray(ys)
        zs = np.asarray(zs)
        if axLabelSign == 0:
            ax1.plot(xs,ys,zs,color='navy',lw=lineWidth,label='Footprints')
            axLabelSign = 1
        else:
            ax1.plot(xs,ys,zs,color='navy',lw=lineWidth)
        # polygon
        verts = [list(zip(xs,ys,zs))]
        ax1.add_collection(Poly3DCollection(verts,alpha=0.6,facecolor='royalblue'))

    axLabelSign = 0
    for foot_t in gps_list_foot_transformed:
        xs = []
        ys = []
        zs = []
        for pt in foot_t:
            #ax2.scatter(pt[0],pt[1],pt[2],marker='^',color='indigo')
            xs.append(pt[0])
            ys.append(pt[1])
            zs.append(pt[2]) 
        xs = np.asarray(xs)
        ys = np.asarray(ys)
        zs = np.asarray(zs)
        if axLabelSign == 0:
            ax2.plot(xs,ys,zs,color='navy',lw=lineWidth,label='Footprints')
            axLabelSign = 1
        else:
            ax2.plot(xs,ys,zs,color='navy',lw=lineWidth)
        # polygon
        verts = [list(zip(xs,ys,zs))]
        ax2.add_collection(Poly3DCollection(verts,alpha=0.6,facecolor='royalblue'))

    # draw walls
    axLabelSign = 0
    for wall in gps_list_wall:
        xs = []
        ys = []
        zs = []
        for pt in wall:
            xs.append(pt[0])
            ys.append(pt[1])
            zs.append(pt[2]) 
        xs = np.asarray(xs)
        ys = np.asarray(ys)
        zs = np.asarray(zs)
        if axLabelSign == 0:
            ax1.plot(xs,ys,zs,color='darkorange',lw=lineWidth,label='Walls')
            axLabelSign = 1
        else:
            ax1.plot(xs,ys,zs,color='darkorange',lw=lineWidth)
        #polygon
        verts = [list(zip(xs,ys,zs))]
        ax1.add_collection(Poly3DCollection(verts,alpha=0.2,facecolor='gold'))

    axLabelSign = 0
    for wall_t in gps_list_wall_transformed:
        xs = []
        ys = []
        zs = []
        for pt in wall_t:
            xs.append(pt[0])
            ys.append(pt[1])
            zs.append(pt[2]) 
        xs = np.asarray(xs)
        ys = np.asarray(ys)
        zs = np.asarray(zs)
        if axLabelSign == 0:
            ax2.plot(xs,ys,zs,color='darkorange',lw=lineWidth,label='Walls')
            axLabelSign = 1
        else:
            ax2.plot(xs,ys,zs,color='darkorange',lw=lineWidth)
        #polygon
        verts = [list(zip(xs,ys,zs))]
        ax2.add_collection(Poly3DCollection(verts,alpha=0.2,facecolor='gold'))

    # Set Equal Boundaries for xyz axis, using exact range of coordinates

    max_range_1 = np.subtract(maxRange_in,minRange_in).max()
    max_range_2 = np.subtract(maxRange_out,minRange_out).max()
    print("rangeDiff_in = ",np.subtract(maxRange_in,minRange_in))
    print("rangeDiff_out = ",np.subtract(maxRange_out,minRange_out))
    print('minRange_in = ',minRange_in)
    print('maxRange_in = ',maxRange_in)
    print('minRange_out = ',minRange_out)
    print('maxRange_out = ',maxRange_out)

    Xb_1 = 0.5*max_range_1*np.mgrid[-1:1:0.5,-0.5:1.5:0.5,-0.5:1.5:0.5][0].flatten() + 0.5*(minRange_in[0]+maxRange_in[0])
    Yb_1 = 0.5*max_range_1*np.mgrid[-1:1:0.5,-0.5:1.5:0.5,-0.5:1.5:0.5][1].flatten() + 0.5*(minRange_in[1]+maxRange_in[1])
    Zb_1 = 0.5*max_range_1*np.mgrid[-1:1:0.5,-0.5:1.5:0.5,-0.5:1.5:0.5][2].flatten() + 0.5*(minRange_in[2]+maxRange_in[2])

    Xb_2 = 0.5*max_range_2*np.mgrid[-1:1:0.5,-0.5:1.5:0.5,-0.5:1.5:0.5][0].flatten() + 0.5*(minRange_out[0]+maxRange_out[0])
    Yb_2 = 0.5*max_range_2*np.mgrid[-1:1:0.5,-0.5:1.5:0.5,-0.5:1.5:0.5][1].flatten() + 0.5*(minRange_out[1]+maxRange_out[1])
    Zb_2 = 0.5*max_range_2*np.mgrid[-1:1:0.5,-0.5:1.5:0.5,-0.5:1.5:0.5][2].flatten() + 0.5*(minRange_out[2]+maxRange_out[2])
    
    
    # change 'w' to 'b*' to view the invisible fake points, and 
    # check if theses fake points are plotted correctly.
    for xb1, yb1, zb1 in zip(Xb_1, Yb_1, Zb_1):
        ax1.plot([xb1], [yb1], [zb1], 'w')
    for xb2, yb2, zb2 in zip(Xb_2, Yb_2, Zb_2):
        ax2.plot([xb2], [yb2], [zb2], 'w')
    
    #polygon: horizon
    xmin,xmax = ax1.get_xlim()
    ymin,ymax = ax1.get_ylim()
    p1 = [xmin,ymin,minRange_in[2]]
    p2 = [xmin,ymax,minRange_in[2]]
    p3 = [xmax,ymax,minRange_in[2]]
    p4 = [xmax,ymin,minRange_in[2]]
    xh = [p1[0],p2[0],p3[0],p4[0],p1[0]]
    yh = [p1[1],p2[1],p3[1],p4[1],p1[1]]
    zh = [p1[2],p2[2],p3[2],p4[2],p1[2]]
    verts = [list(zip(xh,yh,zh))]
    ax1.add_collection(Poly3DCollection(verts,alpha=0.2,facecolor='springgreen'))

    xmin,xmax = ax2.get_xlim()
    ymin,ymax = ax2.get_ylim()
    p1 = [xmin,ymin,minRange_out[2]]
    p2 = [xmin,ymax,minRange_out[2]]
    p3 = [xmax,ymax,minRange_out[2]]
    p4 = [xmax,ymin,minRange_out[2]]
    xh = [p1[0],p2[0],p3[0],p4[0],p1[0]]
    yh = [p1[1],p2[1],p3[1],p4[1],p1[1]]
    zh = [p1[2],p2[2],p3[2],p4[2],p1[2]]
    verts = [list(zip(xh,yh,zh))]
    ax2.add_collection(Poly3DCollection(verts,alpha=0.2,facecolor='springgreen'))

    # Labels
    ax1.set_xlabel('$X$')
    ax1.set_ylabel('$Y$')
    ax1.set_zlabel('$Z$')
    #ax1.axis('equal')
    ax1.legend(bbox_to_anchor=(1.1, 0.9))
    inTitle = inProj.crs.name
    #if you do the inverse operation (UK>>DE), otherwise please comment the next line
    inTitle = outProj.crs.name
    ax1.set_title(inTitle,fontweight="bold")

    ax2.set_xlabel('$X$')
    ax2.set_ylabel('$Y$')
    ax2.set_zlabel('$Z$')
    #ax2.axis('equal')
    ax2.legend(bbox_to_anchor=(1.1, 0.9))
    outTitle = outProj.crs.name
    #if you do the inverse operation (UK>>DE), otherwise please comment the next line
    outTitle = inProj.crs.name
    ax2.set_title(outTitle,fontweight="bold")

    fig.suptitle('CRS Transformation for Roofs & Footprints & Walls',fontsize=16,fontweight="bold")
    fig.savefig("testing_simple_new.png")
    
    print("--- %s seconds ---" % (time.time() - start_time))





