'''
Transform gml coordinates from one CRS to another;
and export to a new gml file.
Drawings're not included at current stage.

Current Process:
	transformation with offset partly completed, with multiprocessing.
However, this script only changes the coordinates value, not including other attributes like srsName.

transfromation of 1 building consumes a time between 1~2 seconds. Slow it is.

'''

import xml.etree.ElementTree as ET
import numpy as np
from pyproj import Proj, transform
import multiprocessing as mp
from multiprocessing import Process, Manager, Queue, Pool
import time
import sys

# A Class for storing data extracted from xml and transformation made by pyproj;
# Use the name property as a reference to find the right place in the original XML file.
class _Building:
	def __init__(self,name):
		self.name = name
		self.roof = []
		self.foot = []
		self.wall = []
#end class _Building

# read xml and save it to cityObjs
def readCityGML(fileName,_nameSpace):
	buildingList = []
	tree = ET.parse(fileName)
	root = tree.getroot()
	for bldg in root.findall('.//bldg:Building',_nameSpace):
		# find the cityObjectMember's name and create an object of Class _Building
		kiminonamae = bldg.attrib
		#print("kiminonamae = ",kiminonamae)
		# The name is the entire attribute of <bldg:Building>, 
		# for example, {'{http://www.opengis.net/gml}id': 'DENW20AL00006aAC'}
		Building = _Building(kiminonamae)
		# seearch the XML file using the XPath format, provided by ElementTree.
		# and change the string into float arrays, stored in the Building object.
		for Pts in bldg.findall('.//bldg:RoofSurface//gml:posList',_nameSpace):
			posList = np.array(str(Pts.text).split(' '))
			posList = posList.astype(np.float)
			Building.roof.append(posList)	
		for Pts in bldg.findall('.//bldg:GroundSurface//gml:posList',_nameSpace):
			posList = np.array(str(Pts.text).split(' '))
			posList = posList.astype(np.float)
			Building.foot.append(posList)
		for Pts in bldg.findall('.//bldg:WallSurface//gml:posList',_nameSpace):
			posList = np.array(str(Pts.text).split(' '))
			posList = posList.astype(np.float)
			Building.wall.append(posList)
		# Append the object Building to our reserved list.
		buildingList.append(Building)
	# end loop of XML searching
	return buildingList

# find the REF point and transform it. 
# In this example, we take the first point in the first GroundSurface as the Reference point.
def getREF(root,_nameSpace,inProj,outProj):
	for obj in root.findall('core:cityObjectMember',_nameSpace):
		for foot in obj.findall('.//bldg:GroundSurface',_nameSpace):
			for pts in foot.findall('.//gml:posList',_nameSpace):
				posList = np.array(str(pts.text).split(' '))
				posList = posList.astype(np.float)
				pt_REF = np.array([posList[0],posList[1],posList[2]])
				pt_REF[0],pt_REF[1] = transform(inProj,outProj,pt_REF[0],pt_REF[1])
				return pt_REF
	return 0
			
# export the xml file
def treeWriter(fileName_exported,tree,buildingList,_nameSpace):
	root = tree.getroot()
	for bldg in root.findall(".//bldg:Building",_nameSpace):
		for building in buildingList:
			if str(bldg.attrib) == str(building.name):
				# roof
				roof_mark = 0
				for pts in bldg.findall(".//bldg:RoofSurface//gml:posList",_nameSpace):	
					transformedList = ['{:.8f}'.format(x) for x in building.roof[roof_mark]]
					seperator = ' '
					pts.text = seperator.join(transformedList)
					roof_mark += 1
				# foot
				foot_mark = 0
				for pts in bldg.findall(".//bldg:GroundSurface//gml:posList",_nameSpace):
					transformedList = ['{:.8f}'.format(x) for x in building.foot[foot_mark]]
					seperator = ' '
					pts.text = seperator.join(transformedList)
					foot_mark += 1
				# wall
				wall_mark = 0
				for pts in bldg.findall(".//bldg:WallSurface//gml:posList",_nameSpace):
					transformedList = ['{:.8f}'.format(x) for x in building.wall[wall_mark]]
					seperator = ' '
					pts.text = seperator.join(transformedList)
					wall_mark += 1		
		# end loop of searching for the building with same name, and go for the next building.
	# end loop of all buildings

	# ElementTree has to register all the nameSpaces(xmlns) manually. Otherwise, the export'll be wrong.
	ET.register_namespace('core','http://www.opengis.net/citygml/1.0')
	ET.register_namespace('gen',"http://www.opengis.net/citygml/generics/1.0")
	ET.register_namespace('grp',"http://www.opengis.net/citygml/cityobjectgroup/1.0")
	ET.register_namespace('app',"http://www.opengis.net/citygml/appearance/1.0")
	ET.register_namespace('bldg',"http://www.opengis.net/citygml/building/1.0")
	ET.register_namespace('gml',"http://www.opengis.net/gml")
	ET.register_namespace('xal',"urn:oasis:names:tc:ciq:xsdschema:xAL:2.0")
	ET.register_namespace('xlink',"http://www.w3.org/1999/xlink")
	ET.register_namespace('xsi',"http://www.w3.org/2001/XMLSchema-instance")
	# change the output file name here:
	tree.write(fileName_exported,xml_declaration=True,encoding='utf-8', method="xml")
	return 0

# CRS transformation, being called repeatedly by multiprocessing's different Processes.
def crsTransform(buildingList,buildingResult,loc,num_of_process,OFFSET,inProj,outProj):
	print("process starts = ",loc)
	# buildingList is read-only, thus, save the transformed xyz results into the buildingResult
	# Now try to pick the building(s) chosen for transformation in ONLY THIS function(process)
	# The argument "loc" differs the buildings to be chosen.
	for i in range(1+int(len(buildingList)/num_of_process)):
		# only select the (i * numOfProc+loc) _th building
		if i*num_of_process+loc >= len(buildingList):
			break
		#single_start_time = time.time()
		proxy = buildingList[i*num_of_process+loc]
		# roof
		for rj in range(len(proxy.roof)):
			# select the j_th roof in ".roof" 
			posList = proxy.roof[rj]
			for k in range(int(len(posList)/3)):
				res_x,res_y = transform(inProj,outProj,posList[3*k], posList[3*k+1])
				res_x = res_x + OFFSET[0]
				res_y = res_y + OFFSET[1]
				proxy.roof[rj][3*k] = res_x
				proxy.roof[rj][3*k+1] = res_y				
		# foot
		for fj in range(len(proxy.foot)):
			posList = proxy.foot[fj]
			for k in range(int(len(posList)/3)):
				res_x,res_y = transform(inProj,outProj,posList[3*k], posList[3*k+1])
				res_x = res_x + OFFSET[0]
				res_y = res_y + OFFSET[1]
				proxy.foot[fj][3*k] = res_x
				proxy.foot[fj][3*k+1] = res_y
		# wall
		for wj in range(len(proxy.wall)): 
			posList = proxy.wall[wj]
			for k in range(int(len(posList)/3)):
				res_x,res_y = transform(inProj,outProj,posList[3*k], posList[3*k+1])
				res_x = res_x + OFFSET[0]
				res_y = res_y + OFFSET[1]
				proxy.wall[wj][3*k] = res_x
				proxy.wall[wj][3*k+1] = res_y
		# save the transformed results 
		buildingResult.append(proxy)
		#print("---In a single loop: %s seconds ---" % (time.time() - single_start_time))
	# end loop for selected buildings
	print("process ends = ",loc)
	return 0

# testing for Pool()
def crsTransformPool(buildingList,buildingResult,loc,OFFSET,inProj,outProj):
	print("process starts = ",loc)	
	#single_start_time = time.time()
	proxy = buildingList[loc]
	# roof
	for rj in range(len(proxy.roof)):
		# select the j_th roof in ".roof" 
		posList = proxy.roof[rj]
		for k in range(int(len(posList)/3)):
			res_x,res_y = transform(inProj,outProj,posList[3*k], posList[3*k+1])
			res_x = res_x + OFFSET[0]
			res_y = res_y + OFFSET[1]
			proxy.roof[rj][3*k] = res_x
			proxy.roof[rj][3*k+1] = res_y				
	# foot
	for fj in range(len(proxy.foot)):
		posList = proxy.foot[fj]
		for k in range(int(len(posList)/3)):
			res_x,res_y = transform(inProj,outProj,posList[3*k], posList[3*k+1])
			res_x = res_x + OFFSET[0]
			res_y = res_y + OFFSET[1]
			proxy.foot[fj][3*k] = res_x
			proxy.foot[fj][3*k+1] = res_y
	# wall
	for wj in range(len(proxy.wall)): 
		posList = proxy.wall[wj]
		for k in range(int(len(posList)/3)):
			res_x,res_y = transform(inProj,outProj,posList[3*k], posList[3*k+1])
			res_x = res_x + OFFSET[0]
			res_y = res_y + OFFSET[1]
			proxy.wall[wj][3*k] = res_x
			proxy.wall[wj][3*k+1] = res_y
	# save the transformed results 
	buildingResult.append(proxy)
	#print("---In a single loop: %s seconds ---" % (time.time() - single_start_time))
	print("process ends = ",loc)
	return 0

def main():
	# target location: values are in target CRS
	# this is a location in London:
	targetLoc = [522901.810984, 179591.260805, 0]

	# in&out projected CRS
	inProj = Proj(init='epsg:5555')
	outProj = Proj(init='epsg:27700')

	# deal with fileNames
	if len(sys.argv) == 1:
		# Default fileNames
		fileName = "DUSSELDORF-5-building-test.xml"
		#fileName = "LoD2_354_5667_1_NW.gml.xml"
		fileName_exported = "testing5.xml"
	elif len(sys.argv) == 2:
		if sys.argv[1] == "-H" or sys.argv[1] == "--help":
			print("-----------------------------------------------------")
			print("python xmlParser_Process.py [input.xml] [output.xml]")
			print("-----------------------------------------------------")
			return 0
		else:
			print("Use \"-H\" or \"--help\" for more information")
			return 0
	elif len(sys.argv) == 3:
		fileName = sys.argv[1]
		fileName_exported = sys.argv[2]
	else:
		print(">>>Error: TOO MANY ARGUMENTS.")
		print("-----------------------------------------------------")
		print("python xmlParser_Process.py [input.xml] [output.xml]")
		print("-----------------------------------------------------")
		return 0

	# nameSpace used for ElementTree's search function
	_nameSpace = {'core':"http://www.opengis.net/citygml/1.0",
	'gen':"http://www.opengis.net/citygml/generics/1.0",
	'grp':"http://www.opengis.net/citygml/cityobjectgroup/1.0",
	'app':"http://www.opengis.net/citygml/appearance/1.0",
	'bldg':"http://www.opengis.net/citygml/building/1.0",
	'gml':"http://www.opengis.net/gml",
	'xal':"urn:oasis:names:tc:ciq:xsdschema:xAL:2.0",
	'xlink':"http://www.w3.org/1999/xlink",
	'xsi':"http://www.w3.org/2001/XMLSchema-instance"}

	# save the xml data into buildingList. To call the building inside it, 
	# use buildingList[x].name or buildingList[x].roof (.foor or .wall) 
	buildingList = readCityGML(fileName,_nameSpace)
	print("Number buildings = ",len(buildingList))

	tree = ET.parse(fileName)

	# find the REF point and OFFSET vector.
	pt_REF = getREF(tree.getroot(),_nameSpace,inProj,outProj)
	OFFSET = np.subtract(np.array(targetLoc),np.array(pt_REF))
	OFFSET[2] = 0

	# prepare for multiprocessing
	# manager.list() is one of several ways that are only available for exchanging data between multple processes. 
	manager = Manager()
	buildingResult = manager.list()

	# determine how many Processes we need for the heavy transformation task.
	# the maximum Processes number is set as 12 here, if "mp.cpu_count()==12";
	# if the building is less than 12, then the number of processes = the number of buildings.
	# For example, a xml file with 100 buildings will call 12 processes to work on the transformation;
	# and each process works on 100/12=8 buildings, or 9 buildings.
	# the workload was equally distributed in all the Processes, and that will be very fast.
	'''
	num_of_building = len(buildingList)
	Max_num_of_process = mp.cpu_count()
	if num_of_building < Max_num_of_process:
		num_of_process = num_of_building
	else:
		num_of_process = Max_num_of_process
	
	# Setup a list of processes that we want to run
	processes = [Process(target=crsTransform, args=(buildingList,buildingResult,loc,num_of_process,OFFSET,inProj,outProj,)) for loc in range(num_of_process)]
	# Run processes
	for i in range(len(processes)):
		processes[i].start()
	# Exit the completed processes
	for j in range(len(processes)):
		processes[j].join()
	'''
	
	#'''
	# You may try Pool(), another method in multiprocessing. No improvement in the performance, 
	# but much simpler in code.
	pool = Pool()
	# use .apply_async()
	#for loc in range(len(buildingList)):
	#	pool.apply_async(crsTransformPool, args=(buildingList,buildingResult,loc,OFFSET,inProj,outProj,))
	# or use starmap(), no difference in the performance.
	pool.starmap(crsTransformPool,[(buildingList,buildingResult,loc,OFFSET,inProj,outProj,) for loc in range(len(buildingList))])
	pool.close()
	pool.join()
	#'''
	# Update buildingList with the buildingResult, which contains the transformation results.
	print("number of results",len(buildingResult))
	print("cpu_count = ",mp.cpu_count())
	for i in range(len(buildingList)):
		building_transformed = buildingResult[i]
		buildingList[i].name = building_transformed.name
		buildingList[i].roof = building_transformed.roof
		buildingList[i].foot = building_transformed.foot
		buildingList[i].wall = building_transformed.wall

	# export the List to an XML
	treeWriter(fileName_exported,tree,buildingList,_nameSpace)

if __name__ == '__main__':
	start_time = time.time()
	main()
	print("--- %.6f seconds ---" % (time.time() - start_time))